Tasks

1. Accomodate with cloning a repository - since you're here, this is almost done
2. Build the existing code using "scons" - use any necessary external resource
3. Try the start_server.sh and start_client.sh (you may start multiple clients) and see how it works
4. Go through the SConstruct/SConscript files and understand what they're doing
5. Go through the RN API in libIxio/include/RN.hpp
6. Go through the udpSample/ directory and familiarize with the structure 
7. Build a similar client/server having the below constraints:

* Changes to files in the libIxia directory should be kept to a minimum (ideally no changes should be done there)
* Replace the UDP sockets with TCP sockets. Accepting new clients by the server should be done asynchronously.
* Use a separate thread for the WaitAndDispatchEvents
* Keep the server ability to serve multiple clients at the same time