#ifndef CLIENT_HPP
#define CLIENT_HPP

#include <string>
#include <deque>
#include <arpa/inet.h>
#include <string.h>

#include "RN.hpp"

class Peer
{
public:
    enum eRole {
        kClient,
        kServer,
    };

    Peer(sockaddr_in &i_serverAddr, eRole i_role);
    ~Peer();

    int Run();
    void SendMessage(sockaddr_in &to, uint8_t *i_message, uint16_t i_messageLen);
    void OnMessageReceived(sockaddr_in &from, uint8_t *i_message, uint16_t i_messageLen);

private:
    //internal struct which will be stored in the message queue
    struct Message {
        uint8_t m_buf[1024];
        uint16_t m_len;
        sockaddr_in m_dest;
        Message()
            :
                m_len(0)
        {
            memset(m_buf, 0, sizeof(m_buf));
        }
    };
    eRole m_role;
    int m_sock;
    RN m_rn;
    //this will be used as destination by the client and as listening address on the server
    sockaddr_in m_serverAddr;
    std::deque<Message> m_messageQueue;

    void FlushMessageQueue();
    bool Write(Message &i_message);
    int IoReadCallback();
    int IoWriteCallback();
    static int IoCallback(rn_pollevent_s *i_event);
};


#endif
