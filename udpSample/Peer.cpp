#include "Peer.hpp"

#include <stdio.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>

#define MAX_FDS 5
#define MAX_EVENTS 10
Peer::Peer(sockaddr_in &i_serverAddr, eRole i_role)
    :
        m_role(i_role),
        m_sock(-1),
        m_rn(MAX_FDS, MAX_EVENTS),
        m_serverAddr(i_serverAddr)
{
    m_sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (m_sock < 0) {
        perror("Failed to create socket");
        return;
    }

    struct sockaddr_in addr;
    if (m_role == kClient) {
        //if this is a client, bind the socket on a random port on loopback IP
        addr.sin_port = 0;
        addr.sin_family = AF_INET;
        inet_pton(AF_INET, "127.0.0.1", &addr.sin_addr);
    } else {
        addr = m_serverAddr;
    }

    int result = bind(m_sock, reinterpret_cast<sockaddr*>(&addr) , sizeof(addr));
    if (result < 0) {
        perror("Failed to bind socket");
    }

    //prepare the socket for non-blocking use
    RN::PrepareFD(m_sock);
    //add the socket to the async IO mechanism
    m_rn.AddIOCallback(m_sock, IoCallback, this);
}

Peer::~Peer()
{
}

#define CLIENT_PING "Ping!"

int Peer::Run()
{
    struct timeval tv;
    int prev = 0;
    gettimeofday(&tv, NULL);
    prev = tv.tv_sec * 1000000 + tv.tv_usec;
    //run loop for both client and server
    //the client sends "Ping!" every second
    //the server just loops through WaitAndDispatchEvents
    while (true) { 
        m_rn.WaitAndDispatchEvents(1000);
        if (m_role == kClient) {
            gettimeofday(&tv, NULL);
            int diff = tv.tv_sec * 1000000 + tv.tv_usec - prev;
            if (diff >= 1000000) {
                SendMessage(m_serverAddr, (uint8_t *)CLIENT_PING, strlen(CLIENT_PING) + 1);
                prev = prev + 1000000;
            }
        }
    }
}

int Peer::IoCallback(rn_pollevent_s *i_event)
{
    int ret = 0;
    Peer *pThis = static_cast<Peer*>(i_event->client.data);

    if (i_event->revents & RN::kError)
        fprintf(stderr, "Problem with the UDP communication for FD %d: %x", i_event->fd, i_event->revents);

    if (i_event->revents & RN::kHangup)
        fprintf(stderr, "PANIC: Hangup %d: %x", i_event->fd, i_event->revents);

    if (i_event->revents & RN::kOutputReady) {
        ret = pThis->IoWriteCallback();
        if (ret)
            fprintf(stderr, "Error occured in IoWriteCallback: %i", ret);
    }

    if (i_event->revents & RN::kInputReady) {
        ret = pThis->IoReadCallback();
        if (ret)
            fprintf(stderr, "Error occured in IoReadCallback: %i", ret);
    }

    return ret;
}

int Peer::IoWriteCallback()
{
    //if we got permission to write, try to flush the message queue
    FlushMessageQueue();
    return 0;
}

int Peer::IoReadCallback()
{
    uint8_t data[1024];
    memset(data, 0, sizeof(data));

    //received input ready event, try to read as much as possible
    for (;;) {
        struct sockaddr_in from;
        socklen_t size = sizeof(from);
        ssize_t count = recvfrom(m_sock, data, sizeof(data), 0, reinterpret_cast<sockaddr *>(&from), &size);

        if (count == -1) {
            if (errno == EAGAIN)
                return 0;
            perror("Error receiving on socket");
            return errno;
        }

        OnMessageReceived(from, data, count);
    }

    return 0;
}

void Peer::SendMessage(sockaddr_in &i_to, uint8_t *i_message, uint16_t i_messageLen)
{
    Message message;
    memcpy(message.m_buf, i_message, i_messageLen);
    message.m_len = i_messageLen;
    message.m_dest = i_to;
    m_messageQueue.push_back(message);
    //when we have a message to be sent, try to flush the message queue
    FlushMessageQueue();
    printf("Sent message to %s:%u (len=%u): %s\n", inet_ntoa(i_to.sin_addr), ntohs(i_to.sin_port), i_messageLen, i_message);
}

#define SERVER_PONG "Pong!"

void Peer::OnMessageReceived(sockaddr_in &from, uint8_t *i_message, uint16_t i_messageLen)
{
    printf("Received message from %s:%u (len=%u): %s\n", inet_ntoa(from.sin_addr), ntohs(from.sin_port), i_messageLen, i_message);
    //when we receive a "Ping!" on the server, respond with a "Pong!"
    if (m_role == kServer) {
        if (strcmp((char *)i_message, CLIENT_PING) == 0) {
            SendMessage(from, (uint8_t *)SERVER_PONG, strlen(SERVER_PONG) + 1);
        }
    }
}

void Peer::FlushMessageQueue()
{
    //this function will try to flush all the message queue
    //it will stop when Write return false
    while (!m_messageQueue.empty()) {
        Message message = m_messageQueue.front();
        if (!Write(message))
            break;
        m_messageQueue.pop_front();
    }
}

bool Peer::Write(Message &i_message)
{
    ssize_t count = sendto(m_sock, i_message.m_buf, i_message.m_len, 0, reinterpret_cast<const sockaddr*>(&i_message.m_dest), sizeof(i_message.m_dest));

    if (count == -1) {
        //sendto will return EAGAIN when it can't send the message. When the socket is ready to send, we will receive an Output Ready event in IoCallback
        if (errno == EAGAIN)
            return false;
        perror("Error writing on socket");
        return false;
    }

    return true;
}

int main(int argc, const char *argv[])
{
    Peer::eRole role = Peer::kClient;
    struct sockaddr_in addr;

    //if this is executed with "-s" we start a server instance
    if (argc >= 2 && strcmp(argv[1], "-s") == 0)
        role = Peer::kServer;

    addr.sin_family = AF_INET;
    addr.sin_port = htons(12345);
    int ret = inet_pton(AF_INET, "127.0.0.1", &addr.sin_addr);
    if (ret < 0) {
        perror("Failed to convert address from string");
        return -1;
    }
    if (ret == 0) {
        fprintf(stderr, "Invalid IPv4 address\n");
        return -1;
    }
    //create the peer and start its main loop
    Peer peer(addr, role);
    peer.Run();
    
    return 0;
}
